﻿using System;
using System.Windows.Forms;

namespace Taschenrechner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //In der Methode btnAddition_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnAddition_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 + zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "+";
        }

        //In der Methode btnSubtraktion_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Im Feld lblErgebnis wird das Resultat ausgegeben. 
        private void btnSubtraktion_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 - zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "-";
        }

        //In der Methode btnMittelwert_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnMittelwert_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = (zahl1 + zahl2) / 2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Mittelwert";
        }

        //In der Methode btnPotenz_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Weil für die Berechnung der Potenz keine Formel erfasst werden kann, gibt es die Möglichkeit sich für die
        //Berechnung der Potenz auf die Bibliothek "Math.Pow" zu beziehen. Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnPotenz_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = Math.Pow(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "^";
        }

        //In der Methode btnMultiplikation_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnMultiplikation_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 * zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "x";
        }

        //In der Methode btnDivision_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnDivision_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 / zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = ":";
        }

        //In der Methode btnMIN_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Weil für die Berechnung vom Minimum keine Formel erfasst werden kann, gibt es die Möglichkeit sich für die
        //Berechnung vom Minimum auf die Bibliothek "Math.Min" zu beziehen. Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnMIN_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = Math.Min(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "MIN";
        }

        //In der Methode btnMAX_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Weil für die Berechnung vom Maximum keine Formel erfasst werden kann, gibt es die Möglichkeit sich für die
        //Berechnung vom Maximum auf die Bibliothek "Math.Max" zu beziehen. Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnMAX_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = Math.Max(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "MAX";
        }

        //In der Methode btnModulo_Click wird definiert, wie die Variablen zahl1 und zahl2 beim Knopfdruck
        //berechnet werden sollen. Das Resultat der Variablen zahl1 und zahl2 wird in der Variable ergebnis berechnet.
        //Im Feld lblErgebnis wird das Resultat ausgegeben.
        private void btnModulo_Click_1(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 % zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Modulo";
        }
    }
}
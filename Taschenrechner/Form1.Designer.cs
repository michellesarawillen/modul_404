﻿namespace Taschenrechner
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOperand1 = new System.Windows.Forms.TextBox();
            this.txtOperand2 = new System.Windows.Forms.TextBox();
            this.lblOperator = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblErgebnis = new System.Windows.Forms.Label();
            this.btnAddition = new System.Windows.Forms.Button();
            this.btnSubtraktion = new System.Windows.Forms.Button();
            this.btnMittelwert = new System.Windows.Forms.Button();
            this.btnPotenz = new System.Windows.Forms.Button();
            this.btnMultiplikation = new System.Windows.Forms.Button();
            this.btnDivision = new System.Windows.Forms.Button();
            this.btnMIN = new System.Windows.Forms.Button();
            this.btnMAX = new System.Windows.Forms.Button();
            this.btnModulo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOperand1
            // 
            this.txtOperand1.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))),
                ((int) (((byte) (255)))), ((int) (((byte) (192)))));
            this.txtOperand1.Location = new System.Drawing.Point(27, 32);
            this.txtOperand1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtOperand1.Name = "txtOperand1";
            this.txtOperand1.Size = new System.Drawing.Size(126, 27);
            this.txtOperand1.TabIndex = 0;
            // 
            // txtOperand2
            // 
            this.txtOperand2.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))),
                ((int) (((byte) (255)))), ((int) (((byte) (192)))));
            this.txtOperand2.Location = new System.Drawing.Point(274, 32);
            this.txtOperand2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtOperand2.Name = "txtOperand2";
            this.txtOperand2.Size = new System.Drawing.Size(126, 27);
            this.txtOperand2.TabIndex = 1;
            // 
            // lblOperator
            // 
            this.lblOperator.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblOperator.Location = new System.Drawing.Point(159, 32);
            this.lblOperator.Name = "lblOperator";
            this.lblOperator.Size = new System.Drawing.Size(109, 28);
            this.lblOperator.TabIndex = 2;
            this.lblOperator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(12, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 28);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ergebnis:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblErgebnis
            // 
            this.lblErgebnis.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))),
                ((int) (((byte) (255)))), ((int) (((byte) (192)))));
            this.lblErgebnis.Location = new System.Drawing.Point(121, 94);
            this.lblErgebnis.Name = "lblErgebnis";
            this.lblErgebnis.Size = new System.Drawing.Size(71, 28);
            this.lblErgebnis.TabIndex = 4;
            // 
            // btnAddition
            // 
            this.btnAddition.Location = new System.Drawing.Point(27, 149);
            this.btnAddition.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddition.Name = "btnAddition";
            this.btnAddition.Size = new System.Drawing.Size(146, 51);
            this.btnAddition.TabIndex = 5;
            this.btnAddition.Text = "Addition";
            this.btnAddition.UseVisualStyleBackColor = true;
            this.btnAddition.Click += new System.EventHandler(this.btnAddition_Click);
            // 
            // btnSubtraktion
            // 
            this.btnSubtraktion.Location = new System.Drawing.Point(226, 149);
            this.btnSubtraktion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSubtraktion.Name = "btnSubtraktion";
            this.btnSubtraktion.Size = new System.Drawing.Size(146, 51);
            this.btnSubtraktion.TabIndex = 6;
            this.btnSubtraktion.Text = "Subtraktion";
            this.btnSubtraktion.UseVisualStyleBackColor = true;
            this.btnSubtraktion.Click += new System.EventHandler(this.btnSubtraktion_Click);
            // 
            // btnMittelwert
            // 
            this.btnMittelwert.Location = new System.Drawing.Point(27, 304);
            this.btnMittelwert.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMittelwert.Name = "btnMittelwert";
            this.btnMittelwert.Size = new System.Drawing.Size(146, 51);
            this.btnMittelwert.TabIndex = 7;
            this.btnMittelwert.Text = "Mittelwert";
            this.btnMittelwert.UseVisualStyleBackColor = true;
            this.btnMittelwert.Click += new System.EventHandler(this.btnMittelwert_Click);
            // 
            // btnPotenz
            // 
            this.btnPotenz.Location = new System.Drawing.Point(226, 304);
            this.btnPotenz.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPotenz.Name = "btnPotenz";
            this.btnPotenz.Size = new System.Drawing.Size(146, 51);
            this.btnPotenz.TabIndex = 8;
            this.btnPotenz.Text = "Potenz";
            this.btnPotenz.UseVisualStyleBackColor = true;
            this.btnPotenz.Click += new System.EventHandler(this.btnPotenz_Click);
            // 
            // btnMultiplikation
            // 
            this.btnMultiplikation.Location = new System.Drawing.Point(27, 228);
            this.btnMultiplikation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMultiplikation.Name = "btnMultiplikation";
            this.btnMultiplikation.Size = new System.Drawing.Size(146, 51);
            this.btnMultiplikation.TabIndex = 9;
            this.btnMultiplikation.Text = "Multiplikation";
            this.btnMultiplikation.UseVisualStyleBackColor = true;
            this.btnMultiplikation.Click += new System.EventHandler(this.btnMultiplikation_Click);
            // 
            // btnDivision
            // 
            this.btnDivision.Location = new System.Drawing.Point(226, 228);
            this.btnDivision.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDivision.Name = "btnDivision";
            this.btnDivision.Size = new System.Drawing.Size(146, 51);
            this.btnDivision.TabIndex = 10;
            this.btnDivision.Text = "Division";
            this.btnDivision.UseVisualStyleBackColor = true;
            this.btnDivision.Click += new System.EventHandler(this.btnDivision_Click);
            // 
            // btnMIN
            // 
            this.btnMIN.Location = new System.Drawing.Point(27, 382);
            this.btnMIN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMIN.Name = "btnMIN";
            this.btnMIN.Size = new System.Drawing.Size(146, 51);
            this.btnMIN.TabIndex = 11;
            this.btnMIN.Text = "MIN";
            this.btnMIN.UseVisualStyleBackColor = true;
            this.btnMIN.Click += new System.EventHandler(this.btnMIN_Click);
            // 
            // btnMAX
            // 
            this.btnMAX.Location = new System.Drawing.Point(226, 382);
            this.btnMAX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMAX.Name = "btnMAX";
            this.btnMAX.Size = new System.Drawing.Size(146, 51);
            this.btnMAX.TabIndex = 12;
            this.btnMAX.Text = "MAX";
            this.btnMAX.UseVisualStyleBackColor = true;
            this.btnMAX.Click += new System.EventHandler(this.btnMAX_Click);
            // 
            // btnModulo
            // 
            this.btnModulo.Location = new System.Drawing.Point(27, 456);
            this.btnModulo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnModulo.Name = "btnModulo";
            this.btnModulo.Size = new System.Drawing.Size(146, 51);
            this.btnModulo.TabIndex = 13;
            this.btnModulo.Text = "Modulo";
            this.btnModulo.UseVisualStyleBackColor = true;
            this.btnModulo.Click += new System.EventHandler(this.btnModulo_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (192)))),
                ((int) (((byte) (255)))));
            this.ClientSize = new System.Drawing.Size(482, 552);
            this.Controls.Add(this.btnModulo);
            this.Controls.Add(this.btnMAX);
            this.Controls.Add(this.btnMIN);
            this.Controls.Add(this.btnDivision);
            this.Controls.Add(this.btnMultiplikation);
            this.Controls.Add(this.btnPotenz);
            this.Controls.Add(this.btnMittelwert);
            this.Controls.Add(this.btnSubtraktion);
            this.Controls.Add(this.btnAddition);
            this.Controls.Add(this.lblErgebnis);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblOperator);
            this.Controls.Add(this.txtOperand2);
            this.Controls.Add(this.txtOperand1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Taschenrechner";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblOperator;
        private System.Windows.Forms.TextBox txtOperand2;
        private System.Windows.Forms.TextBox txtOperand1;
        private System.Windows.Forms.Label lblErgebnis;
        private System.Windows.Forms.Button btnAddition;
        private System.Windows.Forms.Button btnSubtraktion;
        private System.Windows.Forms.Button btnMittelwert;
        private System.Windows.Forms.Button btnPotenz;
        private System.Windows.Forms.Button btnMultiplikation;
        private System.Windows.Forms.Button btnDivision;
        private System.Windows.Forms.Button btnMIN;
        private System.Windows.Forms.Button btnMAX;
        private System.Windows.Forms.Button btnModulo;
    }
}

﻿namespace Schachbrett
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSpielfeld = new System.Windows.Forms.Panel();
            this.cmpDropDown = new System.Windows.Forms.NumericUpDown();
            this.btnSpielStart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.cmpDropDown)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSpielfeld
            // 
            this.pnlSpielfeld.Location = new System.Drawing.Point(50, 165);
            this.pnlSpielfeld.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pnlSpielfeld.Name = "pnlSpielfeld";
            this.pnlSpielfeld.Size = new System.Drawing.Size(400, 400);
            this.pnlSpielfeld.TabIndex = 0;
            // 
            // cmpDropDown
            // 
            this.cmpDropDown.Increment = new decimal(new int[] {2, 0, 0, 0});
            this.cmpDropDown.Location = new System.Drawing.Point(254, 61);
            this.cmpDropDown.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.cmpDropDown.Maximum = new decimal(new int[] {30, 0, 0, 0});
            this.cmpDropDown.Name = "cmpDropDown";
            this.cmpDropDown.Size = new System.Drawing.Size(134, 27);
            this.cmpDropDown.TabIndex = 1;
            // 
            // btnSpielStart
            // 
            this.btnSpielStart.Location = new System.Drawing.Point(54, 59);
            this.btnSpielStart.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnSpielStart.Name = "btnSpielStart";
            this.btnSpielStart.Size = new System.Drawing.Size(188, 41);
            this.btnSpielStart.TabIndex = 2;
            this.btnSpielStart.Text = "Spiel starten";
            this.btnSpielStart.UseVisualStyleBackColor = true;
            this.btnSpielStart.Click += new System.EventHandler(this.btnSpielStart_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 652);
            this.Controls.Add(this.btnSpielStart);
            this.Controls.Add(this.cmpDropDown);
            this.Controls.Add(this.pnlSpielfeld);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Schachbrett";
            ((System.ComponentModel.ISupportInitialize) (this.cmpDropDown)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlSpielfeld;
        private System.Windows.Forms.NumericUpDown cmpDropDown;
        private System.Windows.Forms.Button btnSpielStart;
    }
}

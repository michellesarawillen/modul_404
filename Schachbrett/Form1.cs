﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Schachbrett
{
    public partial class Form1 : Form
    {
        //Mit der Variable X
        private int x = 0;
        private int y = 0;
        //Zähler für Felder vom Schachbrett. x = vertikal, y = horizontal
        private int xCounter = 0;
        private int yCounter = 0;
        //Die Variable isBlack definiert die Farbe der Felder im Schachbrett.
        private bool isBlack = true;

        public Form1()
        {
            InitializeComponent();
        }
        //Beim Klick auf den Startknopf führt die Methode btnSpielStart_Click die Methode clear
        //sowie anschliessend die Methode Zeichneschachbrett aus.
        private void btnSpielStart_Click(object sender, EventArgs e)
        {
            clear();
            ZeichneSchachbrett();
        }
        
        private void ZeichneSchachbrett()
        {
            int size = (int) cmpDropDown.Value; //Die Variable size wird dem Dropdown Feld zugewiesen.
            //Über die Schleife wird die Eingabe der Grösse des Spielfeldes überprüft. Das erste Feld wird schwarz
            //erstellt, somit muss das nächste Feld weiss erstellt werden.
            while (yCounter < size)  
            {
                Label label = new Label();

                if (isBlack == true)
                {
                    label.BackColor = Color.Black;
                }
                else
                {
                    label.BackColor = Color.White;
                }

                isBlack = !isBlack;


                label.Size = new Size(20, 20); //Definiert die Grösse des neu zu erstellenden Feldes
                label.Location = new Point(x, y); //Angabe der Position für das neue Feld


                pnlSpielfeld.Controls.Add(label); //Erstellt das neue Feld auf dem Schachbrett

                x += 20;
                xCounter++;

                if (xCounter == size) //Definiert Wechsel von Schwarz/weiss bei neuer Zeile
                {
                    x = 0;
                    y += 20;
                    xCounter = 0;
                    yCounter++;

                    if (size % 2 == 0)
                    {
                        isBlack = !isBlack;
                    }
                }
            }
        }

        //Spielfeld wird gelöscht
        private void clear()
        {
            x = 0;
            y = 0;
            
            xCounter = 0;
            yCounter = 0;

            isBlack = true;

            pnlSpielfeld.Controls.Clear();

        }

    }
}
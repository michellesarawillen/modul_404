﻿namespace PingPong
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnlSpiel = new System.Windows.Forms.Panel();
            this.picSchlaeger = new System.Windows.Forms.PictureBox();
            this.picBall = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPunkte = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnHoch = new System.Windows.Forms.Button();
            this.btnRunter = new System.Windows.Forms.Button();
            this.btnLinks = new System.Windows.Forms.Button();
            this.btnRechts = new System.Windows.Forms.Button();
            this.pnlSpiel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.picSchlaeger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSpiel
            // 
            this.pnlSpiel.BackColor = System.Drawing.Color.SpringGreen;
            this.pnlSpiel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpiel.Controls.Add(this.picSchlaeger);
            this.pnlSpiel.Controls.Add(this.picBall);
            this.pnlSpiel.Location = new System.Drawing.Point(3, 6);
            this.pnlSpiel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlSpiel.Name = "pnlSpiel";
            this.pnlSpiel.Size = new System.Drawing.Size(456, 341);
            this.pnlSpiel.TabIndex = 0;
            // 
            // picSchlaeger
            // 
            this.picSchlaeger.BackColor = System.Drawing.Color.Black;
            this.picSchlaeger.Location = new System.Drawing.Point(449, 56);
            this.picSchlaeger.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.picSchlaeger.Name = "picSchlaeger";
            this.picSchlaeger.Size = new System.Drawing.Size(4, 40);
            this.picSchlaeger.TabIndex = 1;
            this.picSchlaeger.TabStop = false;
            // 
            // picBall
            // 
            this.picBall.BackColor = System.Drawing.Color.MidnightBlue;
            this.picBall.Location = new System.Drawing.Point(67, 116);
            this.picBall.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picBall.Name = "picBall";
            this.picBall.Size = new System.Drawing.Size(27, 34);
            this.picBall.TabIndex = 0;
            this.picBall.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 434);
            this.btnStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(94, 49);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Spiel starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Location = new System.Drawing.Point(463, 6);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(27, 340);
            this.vScrollBar1.TabIndex = 2;
            this.vScrollBar1.Value = 50;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll_1);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label1.Location = new System.Drawing.Point(12, 368);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 30);
            this.label1.TabIndex = 3;
            this.label1.Text = "Punkte: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPunkte
            // 
            this.txtPunkte.Location = new System.Drawing.Point(86, 370);
            this.txtPunkte.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPunkte.Name = "txtPunkte";
            this.txtPunkte.Size = new System.Drawing.Size(89, 27);
            this.txtPunkte.TabIndex = 4;
            this.txtPunkte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.PowderBlue;
            this.label2.Location = new System.Drawing.Point(174, 400);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(285, 128);
            this.label2.TabIndex = 5;
            this.label2.Text =
                "Tastatursteuerung\r\nTaste\r\nH horizontale Flugrichtung umkehren\r\nV vertikale Flugri" +
                "chtung umkehren\r\nP Spiel pausieren\r\nS Spiel weiterlaufen lassen";
            // 
            // btnHoch
            // 
            this.btnHoch.BackColor = System.Drawing.Color.White;
            this.btnHoch.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("btnHoch.BackgroundImage")));
            this.btnHoch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnHoch.Location = new System.Drawing.Point(588, 132);
            this.btnHoch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHoch.Name = "btnHoch";
            this.btnHoch.Size = new System.Drawing.Size(39, 32);
            this.btnHoch.TabIndex = 6;
            this.btnHoch.UseVisualStyleBackColor = false;
            this.btnHoch.Click += new System.EventHandler(this.btnHoch_Click);
            // 
            // btnRunter
            // 
            this.btnRunter.BackColor = System.Drawing.Color.White;
            this.btnRunter.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("btnRunter.BackgroundImage")));
            this.btnRunter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRunter.Location = new System.Drawing.Point(588, 202);
            this.btnRunter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRunter.Name = "btnRunter";
            this.btnRunter.Size = new System.Drawing.Size(39, 32);
            this.btnRunter.TabIndex = 7;
            this.btnRunter.UseVisualStyleBackColor = false;
            this.btnRunter.Click += new System.EventHandler(this.btnRunter_Click);
            // 
            // btnLinks
            // 
            this.btnLinks.BackColor = System.Drawing.Color.White;
            this.btnLinks.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("btnLinks.BackgroundImage")));
            this.btnLinks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLinks.Location = new System.Drawing.Point(545, 168);
            this.btnLinks.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLinks.Name = "btnLinks";
            this.btnLinks.Size = new System.Drawing.Size(39, 32);
            this.btnLinks.TabIndex = 8;
            this.btnLinks.UseVisualStyleBackColor = false;
            this.btnLinks.Click += new System.EventHandler(this.btnLinks_Click);
            // 
            // btnRechts
            // 
            this.btnRechts.BackColor = System.Drawing.Color.White;
            this.btnRechts.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("btnRechts.BackgroundImage")));
            this.btnRechts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRechts.Location = new System.Drawing.Point(633, 168);
            this.btnRechts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRechts.Name = "btnRechts";
            this.btnRechts.Size = new System.Drawing.Size(39, 32);
            this.btnRechts.TabIndex = 9;
            this.btnRechts.UseVisualStyleBackColor = false;
            this.btnRechts.Click += new System.EventHandler(this.btnRechts_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 552);
            this.Controls.Add(this.btnRechts);
            this.Controls.Add(this.btnLinks);
            this.Controls.Add(this.btnRunter);
            this.Controls.Add(this.btnHoch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPunkte);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.pnlSpiel);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Ping-Pong-Spiel";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.pnlSpiel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.picSchlaeger)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox picBall;
        private System.Windows.Forms.Panel pnlSpiel;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.PictureBox picSchlaeger;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPunkte;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnHoch;
        private System.Windows.Forms.Button btnRechts;
        private System.Windows.Forms.Button btnRunter;
        private System.Windows.Forms.Button btnLinks;
    }
}

﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PingPong
{
    public partial class Form1 : Form
    {
        //Die Variablen _bewegungX (=vertikal) und _bewegungY (=horizontal) definieren, um wie viele Pixel der Ball sich im
        //Spielfeld bewegen soll
        private int _bewegungX = 5;
        private int _bewegungY = 2;


        public Form1()
        {
            InitializeComponent();
        }

        //In der Methode bntStart_Click wird der Timer aufgerufen
        private void btnStart_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        //Über die Methode timer1_Tick wird der Ball in Bewegung gebracht. Auf dem Timer wird der Intervall (wie schnell
        //soll sich der Ball bewegen) gesteuert.
        private void timer1_Tick(object sender, EventArgs e)
        {
            Point location = picBall.Location; //Ballposition herausfinden
            location.X += _bewegungX; //Ball in der vertikale verschieben
            location.Y += _bewegungY; //Ball in der horizontale verschieben
            picBall.Location = location; //Ausgabe der Ballposition


            Size spielfeld = pnlSpiel.Size;
            if (location.Y + picBall.Size.Height > spielfeld.Height || location.Y < 0)
            {
                _bewegungY *= -1;
            }

            if (location.X + picBall.Size.Width > spielfeld.Width || location.X < 0)
            {
                _bewegungX *= -1;

                if (picBall.Location.Y >= picSchlaeger.Location.Y &&
                    picBall.Location.Y <= picSchlaeger.Location.Y + picSchlaeger.Size.Height)
                {
                }
            }
        }

        //Über diese Methode wird der Scrollbar mit dem Schläger verbunden.
        private void vScrollBar1_Scroll_1(object sender, ScrollEventArgs e)
        {
            int maxSchlaegerPosition = pnlSpiel.Size.Height - picSchlaeger.Size.Height;
            int positionProzent = vScrollBar1.Value;
            int position = maxSchlaegerPosition * positionProzent / 100;

            Point schlaegerPosition = picSchlaeger.Location;
            schlaegerPosition.Y = position;
            picSchlaeger.Location = schlaegerPosition;
        }

        //Über diese Methode wird das ganze Formular berücksichtigt, damit es für die Methode ProcessCmdKey keine Rolle
        //spielt, ob im Spielfeld etwas angewählt wurde.
        private void Form1_Load_1(object sender, EventArgs e)
        {
        }

        //Diese Methode definiert was passiert, wenn die Tasten H, V, P oder S gedrückt werden
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.H:
                    _bewegungX *= -1;
                    break;

                case Keys.V:
                    _bewegungY *= -1;
                    break;

                case Keys.P:
                    timer1.Stop();
                    break;

                case Keys.S:
                    timer1.Start();
                    break;
            }


            return base.ProcessCmdKey(ref msg, keyData);
        }

        //In der Methode versetzeBall wird definiert, was mit dem Ball passiert wenn eine der Methoden btnHoch_Click,
        //btnRunter_Click, btnLinks_Click oder btnRechts_Click angeklickt wird.
        private void versetzeBall(String richtung)
        {
            int x = 0;
            int y = 0;

            switch (richtung)
            {
                case "ho":
                    y -= 25;
                    break;

                case "ru":
                    y += 25;
                    break;

                case "li":
                    x -= 25;
                    break;

                case "re":
                    x += 25;
                    break;
            }

            Point ballPosition = picBall.Location;
            ballPosition.Y += y;
            ballPosition.X += x;
            picBall.Location = ballPosition;
        }

        //Durch die Methoden btnHoch_Click, btnRunter_Click, btnLinks_Click und btnRechts_Click wird beim Klick auf die
        //Pfeile die Methode versetzeBall augerufen.
        private void btnHoch_Click(object sender, EventArgs e)
        {
            versetzeBall("ho");
        }

        private void btnRunter_Click(object sender, EventArgs e)
        {
            versetzeBall("ru");
        }

        private void btnLinks_Click(object sender, EventArgs e)
        {
            versetzeBall("li");
        }

        private void btnRechts_Click(object sender, EventArgs e)
        {
            versetzeBall("re");
        }
    }
}
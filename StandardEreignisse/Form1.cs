﻿using System;
using System.Windows.Forms;

namespace StandardEreignisse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Durch die verschiedenen Methoden wird definiert, was bei einem Ereigniss, ausgelöst durch die Maus oder Tastatur,
        //passieren soll. Als Resultat wird jeweils ein entsprechender Text ausgegeben.
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            txtAusgabe.Text += "Klick" + Environment.NewLine;
        }

       private void txtAusgabe_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            txtAusgabe.Text += "Doppelklick" + Environment.NewLine;
        }

       private void txtAusgabe_KeyDown(object sender, KeyEventArgs e)
       {
           txtAusgabe.Text += "Taste runter" + Environment.NewLine;
       }

       private void Form1_KeyUp(object sender, KeyEventArgs e)
       {
           txtAusgabe.Text += "Taste hoch" + Environment.NewLine;
       }

       private void Form1_KeyPress(object sender, KeyPressEventArgs e)
       {
           txtAusgabe.Text += e.KeyChar + " gedrückt" + Environment.NewLine;
       }

       private void Form1_MouseDown(object sender, MouseEventArgs e)
       {
           switch (e.Button)
           {
               case MouseButtons.Left:
                   txtAusgabe.Text += "linke Maustaste runter" + Environment.NewLine;
                   break;
               
               case MouseButtons.Right:
                   txtAusgabe.Text += "rechte Maustaste runter" + Environment.NewLine;
                   break;
               
               case MouseButtons.Middle:
                   txtAusgabe.Text += "mittlere Maustaste runter" + Environment.NewLine;
                   break;
           }
       }

       private void Form1_MouseUp(object sender, MouseEventArgs e)
       
       {
           switch (e.Button)
           {
               case MouseButtons.Left:
                   txtAusgabe.Text += "linke Maustaste hoch" + Environment.NewLine;
               break;
               
               case MouseButtons.Right:
                   txtAusgabe.Text += "rechte Maustaste hoch" + Environment.NewLine;
                   break;
               
               case MouseButtons.Middle:
                   txtAusgabe.Text += "mittlere Maustaste hoch" + Environment.NewLine;
                   break;

           }
           
       }
    }
}
